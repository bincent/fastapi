# 概述
![FastAPI](https://img.shields.io/badge/FastAPI-0.1-blue?style=flat-square)
![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.2.5.RELEASE-brightgreen?style=flat-square)
![Apache 2](https://img.shields.io/badge/License-Apache%202-4EB1BA.svg?style=flat-square)

为接口服务而生，基于SpringBoot完成扩展、自动化配置，快速集成组件，降低学习、使用门槛，提高开发效率。
努力打造一款免费开源、注释全、文档全适合新手学习、方便快速二次开发的框架。

提供了一系列开箱即用的组件，通过封装来简化主流第三方框架的集成，从而提高开发者开发效率、学习成本、降低入门门槛，真正的实现开箱即用！

对SpringBoot简单了解的开发者就可以编写安全稳定的接口服务，可为移动端、网页端等多个端点提供丰富的安全接口。

* 为Java开发者提供低门槛第三方框架集成解决方案，让复杂的框架集成使用的门槛更低。
* 开箱即用，内部封装了主流框架，只需添加依赖、简单配置即可使用。
* 各个组件可独立使用，不再冗余你的应用程序。
* 可简单快速的构建安全的RESTful资源接口服务。
* 可简单快速的构建安全的WebSocket服务。
* 经过全面测试，保证生产环境可靠性。
* 为🇨🇳开源做贡献，希望开源框架可以帮助更多的开发者。

## 项目文档
**官方文档** (https://fastapi.bincent.com)

**源码仓库** (https://gitee.com/bincent/fastapi)

使用vuepress构建，**必备技能：** markdown

**环境安装：**
```shell
# 全局安装
yarn global add vuepress 
# 或 
npm install -g vuepress
```

**编写文档与生成HTML：**
```shell
cd document
vuepress dev .      // 本地运行
vuepress build .    // 生成HTML，将在生成的HTML输出到dist目录
```

## 特别鸣谢
感谢以下的项目,排名不分先后

技术|名称|官网|备注
---|---|---|---
Springboot|springboot框架 ||
MyBatis Generator|代码生成||
PageHelper|MyBatis物理分页插件||
Hikari|数据库连接池||
Log4J|日志组件||
Swagger2|接口测试框架||
Maven|项目构建管理||
Websocket|websocket消息通知||
Kaptcha|google验证码||
Devtools|热部署||
JackSON|Json||
Quartz|定时框架||

## 版权信息
FastAPI遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2020-2020 by [FastAPI](https://fastapi.bincent.com) All rights reserved。
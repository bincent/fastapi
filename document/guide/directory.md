# 目录结构

## 模块介绍
各模块单独为jar包，项目名为 com.bincent.fastapi.模块名称
```
├─common 公共模块
│  ├─base Base继承通用类
│  ├─config springBoot所有配置
│  ├─druid druid连接池
│  ├─exception 异常处理包
│  ├─file 文件上传
│  ├─interceptor 拦截器
│  ├─log 日志记录AOP
│  ├─quartz Spring定时器
│  ├─cache 缓存
│  ├─request 统一输入
│  ├─response 统一输出
│  ├─validate  常用类型检查
│  ├─task 任务包
│  │  ├─async 异步任务
│  └─support 工具包
│
├─orm 公共模块
│  └─mybatis，内置常用CRUD方法
│
├─security 安全模块
│  ├─oauth  OAuth
│  └─jwt  JWT
│
├─document 项目文档，使用vuepress
│  └─dist 生成的html文件
```

## 项目示例
```
├─controller 请求访问模块
│  ├─websocket websoket消息请求
│  └─HomeController.java 首页访问类
│
├─model 实体类模块
│  ├─auto mybatis-generator.xml自动生成实体包
│  └─custom 自定义实体
│
├─service 服务层模块
│
├─util 工具模块
│
├─test 测试类
│
├─resources 配置文件夹
│  
└─pom.xml   maven.xml
```

### 组件
提供多个组件，下面简单介绍组件的用途：
#### 链路日志组件
提供零侵入式、分布式链路日志分析框架使用，可应用到微服务应用内；可根据配置调整日志等级。

#### 安全组件
整合SpringSecurity、JWT、认证授权等来保证接口服务的安全性。

#### ORM组件（数据库持久化组件）
Mybatis，内部提供常用CRUD方法，无需编写一行SQL就可以完成对数据的持久化操作，提供方法命名规则查询、动态查询等特性。

#### 代码生成组件
CodeGen 为数据库表对应生成数据实体生成、动态查询实体，配置数据库链接信息后可根据配置过滤指定的表、表部表、指定前缀的表进行生成。

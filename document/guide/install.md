# 安装

## 构建镜像
**构建镜像**
```
docker build --build-arg=target/*.jar -t bincent/fastapi:latest .
```

**构建完成后启动容器**
```
docker run -p 8080:8080 bincent/fastapi:latest
```
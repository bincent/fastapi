# 缓存配置

缓存参数根据不同的缓存方式会有所区别，通用的缓存参数如下：
```
# 默认缓存驱动
driver: redis
# 缓存连接方式配置
stores:
    redis:
        # 驱动方式
        type: redis
        # 服务器地址
        host: 127.0.0.1
        # 端口
        port: 6379
        # 验证密码
        password:
        # 缓存前缀
        prefix:
        # 缓存有效期 0表示永久缓存
        expire: 0
        # 默认库
        select: 0
        # 超时时间，0为系统配置时间
        timeout: 0
    memcache:
      # 驱动方式
      type: memcache
      # 服务器地址
      host: 127.0.0.1
      # 端口
      port: 11211
      # 账号
      username:
      # 密码
      password:
      # 缓存前缀
      prefix:
      # 缓存有效期 0表示永久缓存
      expire: 0
      # 超时时间，0为系统配置时间
      timeout: 0
    file:
        # 驱动方式
        type: file
        # 缓存保存目录
        path:
        # 缓存前缀
        prefix:
        # 缓存有效期 0表示永久缓存
        expire: 0
# 连接池配置
pool:
  # 开关，不需要设置false
  enable: true
  # 最大连接数，超过将不再新建连接
  max_active: 30
  # 超时时间
  max_wait_time: 5
```
缓存连接方式配置`必带参数`如下：

参数	|描述
---|---
type	|缓存类型
expire	|缓存有效期 （默认为0 表示永久缓存）
prefix	|缓存前缀（默认为空）

连接池的配置参数如下：

参数	|说明
---|---
enable	|开关，不需要设置false
max_active	|最大连接数，超过将不再新建连接
max_wait_time	|超时时间
其中的`max_active`和`max_wait_time`需要根据自身业务和环境进行适当调整，最大化提高系统负载。
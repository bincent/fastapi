# 日志配置
日志的配置文件是配置文件目录下的`log.yml`文件，
系统在进行日志写入之前会读取该配置文件进行初始化。

日志配置支持多通道，默认配置如下：
```
# 默认日志记录通道
channel: file
# 日志记录级别
level:
# 日志类型记录的通道 ['error'=>'email',...]
type_channel:
    info: console
    error: file
# 关闭全局日志写入
close: false
# 日志通道列表
channels:
    file:
      # 日志记录方式
      type: file
      # 日志保存目录
      path:
      # 单文件日志写入
      single: false
      # 独立日志级别
      apart_level:
      # 最大日志文件数量
      max_files: 0
      # 使用JSON格式记录
      json: false
      # 是否实时写入
      realtime_write: false
```
可以添加多个日志通道，每个通道可以设置不同的日志类型。
日志配置参数根据不同的日志类型有所区别，
内置的日志类型包括：`file`，日志类型使用`type`参数配置即可。

日志的全局配置参数包含：

参数	|描述
---|---
default	|默认的日志通道
level	|允许记录的日志级别
type_channel	|日志类型记录的通道

文件类型日志的话，还支持下列配置参数：

参数	|描述
---|---
path	|日志存储路径
file_size	|日志文件大小限制（超出会生成多个文件）
apart_level	|独立记录的日志级别
single	|是否单一文件日志
max_files	|最大日志文件数（超过自动清理 ）
realtime_write	|是否实时写入
# 系统配置文件
下面系统自带的配置文件列表及其作用：

配置文件名	|描述
---|---
cache.yml	|缓存配置
database.yml	|数据库配置
filesystem.yml	|磁盘配置
log.yml	|日志配置

具体的配置参数及默认值可以直接查看应用config目录下面的相关文件内容。
# 数据库配置

```
driver: mysql                  // 默认使用的数据库连接配置
connections:                    // 数据库连接配置信息
    mysql:
        dns:                    // DNS配置方式，如果不为空则以此为值
        type: mysql             // 数据库类型
        hostname: 127.0.0.1     // 服务器地址
        database:               // 数据库名
        username:               // 用户名
        password:               // 密码
        hostport: 3306          // 端口
        params:                 // 数据库连接参数
        charset: utf8mb4        // 数据库编码默认采用utf8
        prefix:                 // 数据库表前缀
        deploy: 0               // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        rw_separate: false      // 数据库读写是否分离 主从式有效
        master_num: 1           // 读写分离后 主服务器数量
        slave_no:               // 指定从服务器序号
        fields_strict: true     // 是否严格检查字段是否存在
        break_reconnect: false  // 是否需要断线重连
        trigger_sql: true       // 监听SQL

    // 更多的数据库配置信息
# 连接池配置
pool:
    # 开关，不需要设置false
    enable: true
    # 最大连接数，超过将不再新建连接
    max_active: 50
    # 超时时间
    max_wait_time: 3
```

连接池的配置参数如下：

参数	|说明
---|---
enable	|开关，不需要设置false
max_active	|最大连接数，超过将不再新建连接
max_wait_time	|超时时间
其中的`max_active`和`max_wait_time`需要根据自身业务和环境进行适当调整，最大化提高系统负载。
# 磁盘配置
文件系统的配置参数如下：
```
# 默认磁盘
driver: local
# 磁盘列表
disks:
    local:
        # 磁盘类型
        type: local
        # 磁盘路径
        root:
        # 磁盘路径对应的外部URL路径
        url: "/storage",
    qiniu:
        # 磁盘类型
        type: qiniu
        # 七牛云的配置,accessKey
        accessKey:
        # 七牛云的配置,secretKey
        secretKey:
        # 七牛云的配置,bucket空间名
        bucket:
        # 七牛云的配置,domain,域名
        domain:
```
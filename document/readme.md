---
home: true
heroImage: /assets/img/fastapi.png
heroText: FastAPI
tagline: 开箱即用，为接口服务而生
actionText: 了解更多 →
actionLink: /guide/
features:
- title: 组件化
  details: 对应集成依赖独立组件化存在
- title: 灵活性
  details: 提供针对第三方依赖特性的封装
- title: 可扩展性
  details: 遵循里氏替换原则,多配置、少编码
footer: Copyright © 2009-2020 Bincent. All Rights Reserved.
---
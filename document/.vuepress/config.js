module.exports = {
    repo: 'https://gitee.com/bincent/fastpi',
    repoLabel: 'Gitee',
    title: 'FastAPI',
    description: '开箱即用，为接口服务而生',
    base: '/',
    dest: './dist',
    ga:'',
    themeConfig: {
        search: false,
        nav: [
            {text: '使用文档', link: '/guide/'},
            {text: '参与贡献', link: '/devote/'},
            {text: '团队介绍', link: '/team'},
        ],
        sidebar: {
            '/guide/':[{
                title: '开始使用',
                collapsable: false,
                children: [
                    '',
                    '/guide/install',
                    '/guide/directory'
                ]
            },{
                title: '配置说明',
                collapsable: false,
                children: [
                    '/guide/config/',
                    '/guide/config/log',
                    '/guide/config/cache',
                    '/guide/config/database',
                    '/guide/config/filesystem',
                ]
            },{
                title: '版本管理',
                collapsable: false,
                children: [
                    '/guide/version/support',
                    '/guide/version/change'
                ]
            }],
            '/devote/':[{
                title: '参与贡献',
                collapsable: false,
                children: [
                    ''
                ]
            },{
                title: '代码规范',
                collapsable: false,
                children: [
                    '/devote/standard/',
                    '/devote/standard/code',
                    '/devote/standard/restful'
                ]
            }]
        }
    }
}
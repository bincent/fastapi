# 开发协作

* 积极参与 [Issue](https://gitee.com/bincent/fastapi/issue) 的讨论，如答疑解惑、提供想法或报告无法解决的错误（Issue）
* 撰写和改进项目的文档（Wiki）
* 提交补丁优化代码（Coding）

参与项目开发，最常用和推荐的首选方式是“Fork + Pull”模式。
在“Fork + Pull”模式下，参与者不必向仓库创建者申请提交权限，
而是在自己的托管空间下建立仓库的派生（Fork）。
至于在派生仓库中创建的提交，可以非常方便地利用 Pull Request 工具向原始仓库的维护者发送 Pull Request。

[可以查看码云帮助文档](https://gitee.com/help/articles/4128#article-header0)
package com.bincent.task;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 异步任务接口
 */
@Component
public interface TaskInterface {
    @Async
    public void execute() throws InterruptedException;
}

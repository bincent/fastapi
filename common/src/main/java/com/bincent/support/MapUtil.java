package com.bincent.support;

import java.util.HashMap;
import java.util.Map;

/**
 * Map构建工具类
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-03 20:40
 */
public class MapUtil {
    private Map<String,Object> map;

    private MapUtil(){
        map = new HashMap<>();
    }

    public static MapUtil builder(){
        return new MapUtil();
    }

    public MapUtil put(String key,Object value){
        this.map.put(key,value);
        return this;
    }

    public Map<String,Object> build(){
        return this.map;
    }
}

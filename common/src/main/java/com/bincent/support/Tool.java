package com.bincent.support;
import org.apache.commons.lang3.StringUtils;
import javax.servlet.http.HttpServletRequest;

/**
 * 系统一般的基础工具
 * 例如  把Integer  Long Double等  参数如果为空 直接转成0
 * 实体类的copy
 * @author Boolean <hongbin.hsu@bincent.com>
 */
public class Tool extends org.springframework.beans.BeanUtils {
    /**
     * 传参 Number 如果 数值为 空 那么直接返回 0
     * @param number
     */
    public static Number isCheckNull(Number number) {
        if(number == null) {
            return 0;
        }else {
            return number;
        }
    }

    public static String getClientIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if(StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
            //多次反向代理后会有多个ip值，第一个ip才是真实ip
            int index = ip.indexOf(",");
            if(index != -1){
                return ip.substring(0,index);
            }else{
                return ip;
            }
        }
        ip = request.getHeader("X-Real-IP");
        if(StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
            return ip;
        }
        return request.getRemoteAddr();
    }
}

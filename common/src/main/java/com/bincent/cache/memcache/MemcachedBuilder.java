package com.bincent.cache.memcache;//package com.bincent.memcache;
//
//import net.rubyeye.xmemcached.MemcachedClient;
//import net.rubyeye.xmemcached.MemcachedClientBuilder;
//import net.rubyeye.xmemcached.XMemcachedClientBuilder;
//import net.rubyeye.xmemcached.utils.AddrUtil;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//
///**
// * Memc
// *
// * @mail:414338069@qq.com
// * @description:
// * @author: chenhao
// * @create: 2020/6/24
// */
//@Configuration
//public class MemcachedBuilder {
//
//    protected static Logger logger = LoggerFactory.getLogger(MemcachedBuilder.class);
//
//    @Resource
//    private XmemcachedConfig xMemcachedProperties;
//
//    @Bean
//    public MemcachedClient getMemcachedClinet(){
//        MemcachedClient memcachedClient = null;
//        try {
//            MemcachedClientBuilder builder = new XMemcachedClientBuilder(AddrUtil.getAddresses(xMemcachedProperties.getServers()));
//            builder.setConnectionPoolSize(xMemcachedProperties.getPoolSize());
//            builder.setOpTimeout(xMemcachedProperties.getOpTimeout());
//            memcachedClient = builder.build();
//        }catch (IOException e){
//            logger.error("init MemcachedClient failed"+e);
//        }
//        return memcachedClient;
//    }
//
//}

package com.bincent.config;

import java.util.Map;

/**
 * 定义磁盘文件配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class FileSystem {
    private String driver = "local";
    private Map disks;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Map getDisks() {
        return disks;
    }

    public void setDisks(Map disks) {
        this.disks = disks;
    }
}

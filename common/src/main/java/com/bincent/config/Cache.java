package com.bincent.config;

import com.bincent.config.cache.Pool;

import java.util.Map;

/**
 * 定义缓存配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class Cache {
    private String driver = "redis";
    private Map stores;
    private Pool pool;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Map getStores() {
        return stores;
    }

    public void setStores(Map stores) {
        this.stores = stores;
    }

    public Pool getPool() {
        return pool;
    }

    public void setPool(Pool pool) {
        this.pool = pool;
    }
}

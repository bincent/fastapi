package com.bincent.config;

import com.bincent.config.database.Pool;

import java.util.Map;

/**
 * 定义数据库配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class Database {
    private String driver = "mysql";
    private Map connections;
    private Pool pool;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Map getConnections() {
        return connections;
    }

    public void setConnections(Map connections) {
        this.connections = connections;
    }

    public Pool getPool() {
        return pool;
    }

    public void setPool(Pool pool) {
        this.pool = pool;
    }
}

package com.bincent.config;

/**
 * 定义连接池抽像
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
abstract public class PoolAbstract {
    protected boolean enable = false; // 开关，不需要设置false
    protected int max_active = 50; // 最大连接数，超过将不再新建连接
    protected int max_wait_time = 3; // 超时时间

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getMax_active() {
        return max_active;
    }

    public void setMax_active(int max_active) {
        this.max_active = max_active;
    }

    public int getMax_wait_time() {
        return max_wait_time;
    }

    public void setMax_wait_time(int max_wait_time) {
        this.max_wait_time = max_wait_time;
    }

}

package com.bincent.config.database;

/**
 * 定义用于接收 MySQL 配置文件
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-03 19:55
 */
public class MySQL extends AbstractDatabase {
    protected String type = "mysql";
    protected String charset = "utf8mb4";
}
package com.bincent.config.database;

/**
 * 定义用于接收 PostgreSQL 配置文件
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-03 23:51
 */
public class PostgreSQL extends AbstractDatabase {
    protected String type = "postgresql";


}

package com.bincent.config.database;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 定义用于接收 数据库连接 配置文件，
 * 支持多数据库连接配置信息
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-03 19:55
 */
@Component
@PropertySource(value = "classpath:config/database.yml", encoding = "UTF-8")
@ConfigurationProperties(prefix = "database")
abstract public class AbstractDatabase {
    protected String type = "mysql";
    /**
     *  连接dsn
     */
    protected String dns;
    /**
     * 服务器地址
     */
    protected String hostname;
    /**
     * 数据库名
     */
    protected String database;
    /**
     * 用户名
     */
    protected String username;
    /**
     *  密码
     */
    protected String password;
    /**
     *  端口
     */
    protected int hostport;
    /**
     * 数据库编码默认采用utf8
     */
    protected String charset = "utf8";
    /**
     * 数据库表前缀
     */
    protected String prefix = "";
    /**
     * 数据库连接参数
     */
    protected Map<String,Object> params;
    /**
     * 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
     */
    protected int deploy = 0;
    /**
     * 数据库读写是否分离 主从式有效
     */
    protected boolean rwSeparate = false;
    /**
     * 读写分离后 主服务器数量
     */
    protected int masterNum = 1;
    /**
     * 指定从服务器序号
     */
    protected String slaveNo = "";
    /**
     * 模型写入后自动读取主服务器
     */
    protected boolean readMaster = false;
    /**
     * 是否严格检查字段是否存在
     */
    protected boolean fieldsStrict = true;
    /**
     * 开启字段缓存
     */
    protected boolean fieldsCache = false;
    /**
     * 监听SQL
     */
    protected boolean triggerSql = true;
    /**
     * 是否需要断线重连
     */
    protected boolean breakReconnect = false;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDns() {
        return dns;
    }

    public void setDns(String dns) {
        this.dns = dns;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getHostport() {
        return hostport;
    }

    public void setHostport(int hostport) {
        this.hostport = hostport;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int getDeploy() {
        return deploy;
    }

    public void setDeploy(int deploy) {
        this.deploy = deploy;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public boolean isRwSeparate() {
        return rwSeparate;
    }

    public void setRwSeparate(boolean rwSeparate) {
        this.rwSeparate = rwSeparate;
    }

    public int getMasterNum() {
        return masterNum;
    }

    public void setMasterNum(int masterNum) {
        this.masterNum = masterNum;
    }

    public String getSlaveNo() {
        return slaveNo;
    }

    public void setSlaveNo(String slaveNo) {
        this.slaveNo = slaveNo;
    }

    public boolean isReadMaster() {
        return readMaster;
    }

    public void setReadMaster(boolean readMaster) {
        this.readMaster = readMaster;
    }

    public boolean isFieldsStrict() {
        return fieldsStrict;
    }

    public void setFieldsStrict(boolean fieldsStrict) {
        this.fieldsStrict = fieldsStrict;
    }

    public boolean isFieldsCache() {
        return fieldsCache;
    }

    public void setFieldsCache(boolean fieldsCache) {
        this.fieldsCache = fieldsCache;
    }

    public boolean isTriggerSql() {
        return triggerSql;
    }

    public void setTriggerSql(boolean triggerSql) {
        this.triggerSql = triggerSql;
    }

    public boolean isBreakReconnect() {
        return breakReconnect;
    }

    public void setBreakReconnect(boolean breakReconnect) {
        this.breakReconnect = breakReconnect;
    }
}
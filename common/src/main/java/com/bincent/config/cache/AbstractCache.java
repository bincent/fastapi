package com.bincent.config.cache;

/**
 * 缓存连接方式配置必带参数
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 10:58
 */
abstract public class AbstractCache {
    /**
     * 驱动方式
     */
    private String type = "redis";
    /**
     * 缓存有效期 （默认为0 表示永久缓存）
     */
    private int expire = 0;
    /**
     * 缓存前缀（默认为空）
     */
    private String prefix = "";


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getExpire() {
        return expire;
    }

    public void setExpire(int expire) {
        this.expire = expire;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}

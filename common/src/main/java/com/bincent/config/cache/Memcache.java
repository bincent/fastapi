package com.bincent.config.cache;

import java.util.Map;

/**
 * 定义用于接收 Memcached 配置文件
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-03 19:55
 */
public class Memcache extends AbstractCache {
    // 驱动方式
    protected String type = "memcache";
    protected String host = "127.0.0.1";
    protected int port = 11211;
    // 超时时间（单位：毫秒）
    protected int timeout = 0;
    //账号
    protected String username = "";
    //密码
    protected String password = "";
    protected Map<String,Object> option;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Object> getOption() {
        return option;
    }

    public void setOption(Map<String, Object> option) {
        this.option = option;
    }
}

package com.bincent.config.cache;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 定义用于接收 Redis 配置文件
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-03 19:55
 */
@Configuration
@PropertySource(value = "classpath:config/cache.yml", encoding = "UTF-8")
@ConfigurationProperties(prefix = "redis")
public class Redis extends AbstractCache {
    protected String    type = "redis"; // 驱动方式
    protected String    host = "127.0.0.1";
    protected int       port = 6379;
    protected String    password = "";  // 密码
    protected int       timeout = 0;    // 超时时间（单位：毫秒）
    protected int       select = 0; // 默认库
    protected boolean   persistent = false;  // 持久

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
    }

    public boolean isPersistent() {
        return persistent;
    }

    public void setPersistent(boolean persistent) {
        this.persistent = persistent;
    }
}

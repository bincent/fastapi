package com.bincent.config.cache;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Xmemcached 配置文件
 *
 * @mail:414338069@qq.com
 * @description:
 * @author: chenhao
 * @create: 2020/6/24
 */
@Component
@ConfigurationProperties(prefix = "memcached")
@PropertySource(value = "classpath:config/cache.yml", encoding = "UTF-8")
public class XmemcachedConfig {
    private String servers;
    private int poolSize;
    private long opTimeout;

    public String getServers() {
        return servers;
    }

    public void setServers(String servers) {
        this.servers = servers;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public long getOpTimeout() {
        return opTimeout;
    }

    public void setOpTimeout(long opTimeout) {
        this.opTimeout = opTimeout;
    }
}


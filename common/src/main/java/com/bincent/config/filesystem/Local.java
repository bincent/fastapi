package com.bincent.config.filesystem;

/**
 * 定义本地磁盘文件配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class Local extends FileSystemAbstract {
    protected String type = "local";
    private String root; // 磁盘路径
    private String url; // 磁盘路径对应的外部URL路径
}

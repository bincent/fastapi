package com.bincent.config.filesystem;

/**
 * 定义七牛磁盘文件配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class Qiniu extends FileSystemAbstract {
    protected String type = "qiniu";
    protected String accessKey;
    protected String secretKey;
    protected String bucket;
    protected String domain;
}

package com.bincent.config.filesystem;

/**
 * 定义磁盘文件配置抽像
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
abstract public class FileSystemAbstract {
    protected String type = "local";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

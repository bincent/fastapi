package com.bincent.config;

import java.util.Map;

/**
 * 定义日志配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class Log {
    private String channel = "file";
    private Map level;
    private Map type_channel;
    private boolean close;
    private Map channels;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Map getLevel() {
        return level;
    }

    public void setLevel(Map level) {
        this.level = level;
    }

    public Map getType_channel() {
        return type_channel;
    }

    public void setType_channel(Map type_channel) {
        this.type_channel = type_channel;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public Map getChannels() {
        return channels;
    }

    public void setChannels(Map channels) {
        this.channels = channels;
    }
}

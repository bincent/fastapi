package com.bincent.config.log;

/**
 * 定义日志文件配置
 *
 * @Version 1.0
 * @License Apache2
 * @Author Boolean <hongbin.hsu@bincent.com> since.
 * @DateTime: 2020-07-05 11：05
 */
public class File {
    protected String type = "file";
    protected String path; // 日志保存目录
    protected boolean single = false; // 单文件日志写入
    protected String apart_level; // 独立日志级别
    protected int max_files = 0; // 最大日志文件数量
    protected boolean json = false; // 使用JSON格式记录
    protected boolean realtime_write = false; //是否实时写入

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isSingle() {
        return single;
    }

    public void setSingle(boolean single) {
        this.single = single;
    }

    public String getApart_level() {
        return apart_level;
    }

    public void setApart_level(String apart_level) {
        this.apart_level = apart_level;
    }

    public int getMax_files() {
        return max_files;
    }

    public void setMax_files(int max_files) {
        this.max_files = max_files;
    }

    public boolean isJson() {
        return json;
    }

    public void setJson(boolean json) {
        this.json = json;
    }

    public boolean isRealtime_write() {
        return realtime_write;
    }

    public void setRealtime_write(boolean realtime_write) {
        this.realtime_write = realtime_write;
    }
}
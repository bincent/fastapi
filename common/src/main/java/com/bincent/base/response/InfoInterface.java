package com.bincent.base.response;

public interface InfoInterface {
    int getCode();
    String getMessage();
}

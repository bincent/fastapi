package com.bincent.base.response;

public class Body {
    private int code;
    private String message;
    private Object data;

    public void Failed(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public void Success (Object data) {
        this.code = 200;
        this.message = "success";
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

package com.bincent.base.response;

public class InfoException extends Exception{
    private InfoInterface infoInterface;

    public InfoException(InfoInterface infoInterface) {
        this.infoInterface = infoInterface;
    }

    public InfoInterface getInfoInterface() {
        return infoInterface;
    }

    public void setInfoInterface(InfoInterface infoInterface) {
        this.infoInterface = infoInterface;
    }
}